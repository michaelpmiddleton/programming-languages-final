--[[
    FILE:       tile.lua
    AUTHOR:     mmiddleton
    DATE:       6 MAY 2018

    DESCRIPTION:
    This code comprises the Tile object which holds information and methods pertaining to
    grid cells on the gameboard
]]--
module (..., package.seeall)

function new ()
    object = {}

    object.occupied = false
    object.color = nil

    object.Occupy = function (self, color)
        self.occupied = true
        self.color = color
    end

    object.Clear = function (self)
        self.occupied = false
        self.color = nil
    end

    return object
end