--[[
    FILE:       player.lua
    AUTHOR:     mmiddleton
    DATE:       5 MAY 2018

    DESCRIPTION:
    This file comprises all the code for the player object
]]--
module (..., package.seeall)
require 'math'

function new ()
    object = {}
    
    object.score = 0

    -- IncrementScore():    Uses the piece.color as an ID for the amount of points
    object.IncrementScore = function (self, exponent)
        value = 0

        if (exponent == 1) then
            value = 100

        elseif (exponent > 1) then
            value = 100
            
            for c = 1, exponent do
                value = value * 2
            end

        end
        
        self.score = self.score + value
    end

    return object
end