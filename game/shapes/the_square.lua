--[[
    FILE:       shapes/the_square.lua
    AUTHOR:     mmiddleton
    DATE:       7 MAY 2018

    DESCRIPTION:

    - - - -
    - - - -     Perm. 2, 3, & 4 = Perm. 1
    X X - - 
    X X - - 

]]--
module (..., package.seeall)

require "config"

function new ()
    local object = {}

    object.coords =  {
        [1] = {1, 3},
        [2] = {2, 3},
        [3] = {1, 4},
        [4] = {2, 4}
    }
    object.color = 4
    object.permutationIndex = 1

    object.Rotate = function (self) end

    return object
end