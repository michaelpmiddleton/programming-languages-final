--[[
    FILE:       shapes/the_l.lua
    AUTHOR:     mmiddleton
    DATE:       7 MAY 2018

    DESCRIPTION:

    - - - -     - - - -     - - - -     - - - -
    X - - -     - - - -     X X - -     - - - -
    X - - -     X X X -     - X - -     - - X -
    X X - -     X - - -     - X - -     X X X -

]]--
module (..., package.seeall)

require "config"

local PERMUTATIONS = {
    {
        [1] = {1, 2},
        [2] = {1, 3},
        [3] = {1, 4},
        [4] = {2, 4}
    },
    {
        [1] = {1, 3},
        [2] = {2, 3},
        [3] = {3, 3},
        [4] = {1, 4}
    },
    {
        [1] = {1, 2},
        [2] = {2, 2},
        [3] = {2, 3},
        [4] = {2, 4}
    },
    {
        [1] = {3, 3},
        [2] = {1, 4},
        [3] = {2, 4},
        [4] = {3, 4}
    }
}

local TOTAL_PERMUTATIONS = 4

function new ()
    local object = {}
    
    object.coords = PERMUTATIONS[1]
    object.color = 3
    object.permutationIndex = 1

    object.Rotate = function (self)
        self.permutationIndex = (self.permutationIndex % TOTAL_PERMUTATIONS) + 1
        self.coords = PERMUTATIONS[self.permutationIndex]
    end

    return object
end