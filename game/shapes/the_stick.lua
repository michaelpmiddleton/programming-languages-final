--[[
    FILE:       shapes/the_stick.lua
    AUTHOR:     mmiddleton
    DATE:       7 MAY 2018

    DESCRIPTION:

    X - - -     - - - -
    X - - -     - - - -     Perm. 3 = Perm. 1       Perm. 4 = Perm. 2
    X - - -     - - - - 
    X - - -     X X X X 

]]--
module (..., package.seeall)

require "config"

local PERMUTATIONS = {
    {
        [1] = {1, 1},
        [2] = {1, 2},
        [3] = {1, 3},
        [4] = {1, 4}
    },
    {
        [1] = {1, 4},
        [2] = {2, 4},
        [3] = {3, 4},
        [4] = {4, 4}
    }
}

local TOTAL_PERMUTATIONS = 2

function new ()
    local object = {}

    object.coords = PERMUTATIONS[1]
    object.color = 1
    object.permutationIndex = 1

    object.Rotate = function (self)
        self.permutationIndex = (self.permutationIndex % TOTAL_PERMUTATIONS) + 1
        self.coords = PERMUTATIONS[self.permutationIndex]
    end

    return object
end