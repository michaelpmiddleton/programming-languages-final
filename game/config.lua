--[[
    FILE:       config.lua
    AUTHOR:     mmiddleton
    DATE:       5 MAY 2018

    DESCRIPTION:
    This file contains variables and objects that will be needed by multiple/all
    classes in the project.
]]--

Config = {
    -- Size of the screen used for the full screen
    SCREEN_WIDTH = 1280;
    SCREEN_HEIGHT = 896;

    -- Size of the gameboard
    BOARD_HEIGHT = 28;
    BOARD_WIDTH = 10;

    DEBUG = false;

    -- Colors to be used in love.draw()
    gameColors = {
        ["Menu_Text"] = {255, 255, 255, 255},
        ["Menu_Text_Hover"] = {0, 0, 0, 255},
        ["Menu_Cursor"] = {180, 180, 180, 150}
    };

    -- Colors to be used for the shapes in-game
    shapeColors = {
        [1] = {0, 255, 255},    -- CYAN
        [2] = {0, 0, 255},      -- BLUE
        [3] = {255, 164, 3},    -- ORANGE
        [4] = {255, 255, 4},    -- YELLOW
        [5] = {0, 255, 3},      -- GREEN
        [6] = {127, 0, 127},    -- PURPLE/MAGENTA
        [7] = {255, 0, 2}       -- RED
    };
}