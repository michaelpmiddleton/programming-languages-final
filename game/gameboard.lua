--[[
    FILE:       gameboard.lua
    AUTHOR:     mmiddleton
    DATE:       7 MAY 2018

    DESCRIPTION:
    This file comprises all of the code pertaining to the logic of the gameboard.
]]--

require "config"

local inspect = require 'inspect'
local tile = require "tile"

local config = Config

Gameboard = {
    -- Variables: ---------------------------------------------------
    -- "Constants"
    START_X = 400;
    FINISH_X = config.SCREEN_WIDTH * (2 / 3) + 100;
    CELL_WIDTH = nil;        -- These constants need to be set in init because they require access to the above variables
    CELL_HEIGHT = nil;       -- ^^^
    
    -- Other variables:
    grid = nil;          -- 2D array full of 0's and 1's to denote free or occupied spaces on the board, respectively


    -- Functions: ---------------------------------------------------
    -- Initialize():    "Constructor" for the gameboard object
    Initialize = function (self)
        self.grid = nil
        self.grid = {}
        self.CELL_WIDTH = (self.FINISH_X - self.START_X) / config.BOARD_WIDTH
        self.CELL_HEIGHT = (config.SCREEN_HEIGHT / config.BOARD_HEIGHT)

        for i = 1, config.BOARD_WIDTH do
            self.grid[i] = {}
            
            for j = 1, config.BOARD_HEIGHT do
                self.grid[i][j] = tile.new ()
            end
        end
    end;






    -- SetTiles():  Takes the gamelevel.currentPiece and sets the corresponding grid cells to the
    SetTiles = function (self, coords, color)
        for c = 1, 4 do
            self.grid[coords[c][1]][coords[c][2]]:Occupy (color)
        end
    end;





    -- CheckTile():     Returns whether or not the given Tile at grid[X][Y] is occupied
    CheckTile = function (self, coords)
        return self.grid[coords[1]][coords[2]].occupied
    end;




    -- BreakLine():     Clears the line for grid[c][lineToBreak] and pushes everything above it down one line
    BreakLine = function (self, lineToBreak)
        for c = 1, config.BOARD_WIDTH do
            self.grid[c][lineToBreak]:Clear ()
        end
        
        
        for j = 1, lineToBreak - 1 do
            for i = 1, config.BOARD_WIDTH do
                if (self.grid[i][lineToBreak - j].occupied) then
                    self.grid[i][lineToBreak - j + 1]:Occupy (self.grid[i][lineToBreak - j].color)
                    self.grid[i][lineToBreak - j]:Clear ()
                end 
            end
        end
    end;





    -- ProcessBreaks():     Checks if there is a break to be found. If so, deletes the row(s) and shifts the grid accordingly.
    ProcessBreaks = function (self, score)
        madeBreak = false

        for j = 1, config.BOARD_HEIGHT do
            for i = 1, config.BOARD_WIDTH do
                -- If at any point we see an unoccupied grid space, it's not a breakable line
                if (not self.grid[i][config.BOARD_HEIGHT - j + 1].occupied) then
                    break
                end

                -- If we got to this point and we're equal to BOARD_WIDTH, this is a breakable line
                if (i == config.BOARD_WIDTH) then
                    self:BreakLine (config.BOARD_HEIGHT - j + 1)    -- Break the line
                    score = self:ProcessBreaks (score + 1)          -- Make recursive call as it's possible that there's another break to be processed
                end
            end
        end

        return score
    end;





    -- Display():   Makes calls to love.graphics to display gameboard data graphically
    Display = function (self)
        love.graphics.setColor (config.gameColors["Menu_Text"])

        -- Left outline
        love.graphics.rectangle (
            'fill',
            self.START_X - 5,
            0,
            5,
            config.SCREEN_HEIGHT
        )

        -- Right outline
        love.graphics.rectangle (
            'fill',
            self.FINISH_X,
            0,
            5,
            config.SCREEN_HEIGHT
        )
                                
        
        -- DEBUG:   Show the gameboard grid
        if config.DEBUG then
            for i = 1, config.BOARD_WIDTH do
                love.graphics.line (
                    (self.CELL_WIDTH * i) + self.START_X,
                    0,
                    (self.CELL_WIDTH * i) + self.START_X,
                    config.SCREEN_HEIGHT
                )
            end

            for j = 1, config.BOARD_HEIGHT do
                love.graphics.line (
                    self.START_X,
                    self.CELL_HEIGHT * j,
                    self.FINISH_X,
                    self.CELL_HEIGHT * j
                )
            end
        end


        -- Draw the static-shapes ('j' starts at 5 because the first 4 are empty for creating a new shape to drop)
        for i = 1, config.BOARD_WIDTH do
            for j = 5, config.BOARD_HEIGHT do
                if (self.grid[i][j].occupied) then
                    love.graphics.setColor (config.shapeColors[self.grid[i][j].color])

                    love.graphics.rectangle (
                        'fill',
                        (self.CELL_WIDTH * (i-1)) + self.START_X,
                        self.CELL_HEIGHT * (j - 1),
                        self.CELL_WIDTH,
                        self.CELL_HEIGHT
                    )                        
                end
            end
        end
    end;
}