--[[
    FILE:       sound_manager.lua
    AUTHOR:     mmiddleton
    DATE:       11 MAY 2018

    DESCRIPTION:
    This file comprises the code for the object that manages sound in the project
]]--
module (..., package.seeall)

local SOUNDS = {
    ["music"] = love.audio.newSource ('assets/music.mp3'),
    ["sfx_menuSelect"] = love.audio.newSource ('assets/menu_select.wav', 'static'),
    ["sfx_menuMove"] = love.audio.newSource ('assets/menu_move.wav', 'static'),
    ["sfx_gameStart"] = love.audio.newSource ('assets/game_start.wav', 'static'),
    ["sfx_gameOver"] = love.audio.newSource ('assets/game_over.wav', 'static'),
    ["sfx_lineBreak"] = love.audio.newSource ('assets/line_break.wav', 'static')
}

local MUTE = false

function new ()
    object = {}

    SOUNDS["music"]:setLooping (true)

    object.Play = function (self, audioID)
        if (not MUTE) then
            SOUNDS[audioID]:play ()
        end
    end

    object.Pause = function (self, audioID)
        SOUNDS[audioID]:pause ()
    end

    return object
end