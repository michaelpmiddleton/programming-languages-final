--[[
    FILE:       main.lua
    AUTHOR:     mmiddleton
    DATE:       15 APR 2018

    DESCRIPTION:
    This file is the main menu for the game.
]]--

-- Import the game level
require "level"
require "config"


-- Create a GameLevel object to be used when the player selects "Play"
local game = GameLevel      -- level.lua
local config = Config       -- config.lua

-- Options in the main menu
local mainMenuOptions = {
    "Play",
    "Options",
    "Credits",
    "Exit"
}

-- The index of the menu cursor. (We start at 1 because Lua starts indices at 1, not 0)
local cursorIndex = 1;




--[[
    love.load():
    https://love2d.org/wiki/love.load
]]--
function love.load ()
    love.window.setMode (config.SCREEN_WIDTH, config.SCREEN_HEIGHT, {resizable=false, fullscreen=true})    

    game:Initialize ()
    game.sounds:Play ('music')
end    





--[[
    love.draw():
    https://love2d.org/wiki/love.draw
]]--
function love.draw ()
    -- If the game is in session, display the game 
    if game.inSession then
        game:Display ()

    -- Else, display the main menu (TODO: Change this to handle the credits and how-to screen)
    else
        DisplayMainMenu ()
    end
end







--[[
    love.update():
    https://love2d.org/wiki/love.update    
]]--
function love.update (deltaTime)
    if game.inSession then
        game:Update (deltaTime)
    end
end




--[[
    DisplayMainMenu():
    Contains the code that will display the main menu.
]]--
function DisplayMainMenu ()
    love.graphics.setColor (config.gameColors.Menu_Text)

    for index,option in ipairs (mainMenuOptions) do
        if (index ~= cursorIndex) then
            -- Print the different menu options, starting at 1/3 of screen height and offset by 35.
            love.graphics.print (option,
                10,
                (((config.SCREEN_HEIGHT / 5) * 2) + ((index - 1) * 35)),
                0,
                2,
                2
            )
        end
    end

    

    -- Display the cursor on the menu:
    love.graphics.setColor (config.gameColors.Menu_Cursor)
    love.graphics.rectangle (
        'fill',
        5,
        ((config.SCREEN_HEIGHT / 5) * 2) + ((cursorIndex - 1) * 35) - 5,
        205,
        40
    )


    
    -- Draw negative color for the selected text:
    love.graphics.setColor (config.gameColors.Menu_Text_Hover)
    love.graphics.print (
        mainMenuOptions[cursorIndex],
        10,
        (((config.SCREEN_HEIGHT / 5) * 2) + ((cursorIndex - 1) * 35)),
        0,
        2,
        2
    )
end



--[[
    ProcessMenuRequest():
    Contains the code that handles main menu interactions.
]]--
function ProcessMenuRequest ()
    game.sounds:Play ('sfx_menuSelect')

    if cursorIndex == 1 then
        game:Initialize ()
        game:Play ()
    
    elseif cursorIndex == 4 then
        love.event.quit (0)

    else
        print ("TODO - Menu Option \t" .. mainMenuOptions[cursorIndex])
    end
end



--[[
    love.keypressed():
    https://love2d.org/wiki/love.keypressed
]]--
function love.keypressed (key)
    if key == 'down' then 
        if cursorIndex < table.getn (mainMenuOptions) then
            cursorIndex = cursorIndex + 1
            game.sounds:Play ('sfx_menuMove')            
        end
    end

    if key == 'up' then 
        if cursorIndex > 1 then
            cursorIndex = cursorIndex - 1
            game.sounds:Play ('sfx_menuMove')
        end
    end

    if key == 'left' then
        if (game.inSession) then
            if (game.cursor[1] > 1) then
                game.cursor[1] = game.cursor[1] - 1
            end
        end
    end

    if key == 'right' then
        if (game.inSession) then
            canMoveRight = true

            if (game.cursor[1] < config.BOARD_WIDTH) then
                for c = 1, 4 do
                    if ((game.cursor[1] + game.currentPiece.coords[c][1] - 1) >= config.BOARD_WIDTH) then
                        canMoveRight = false
                        break
                    end
                end

                if (canMoveRight) then
                    game.cursor[1] = game.cursor[1] + 1
                end
            end
        end
    end

    if key == 'return' then 
        if (game.inSession) then
            if (game.gameOver) then
                game:Play ()
            end
        else
            ProcessMenuRequest ()
        end
    end

    if key == '/' then
        game:SavePiece ()
    end

    if key == 'rshift' then
        game.rotateRequest = true 
    end

    if key == 'escape' then
        if (game.inSession) then
            game:Quit()
        end
    end
end