--[[
    FILE:       level.lua
    AUTHOR:     mmiddleton
    DATE:       15 APR 2018

    DESCRIPTION:
    This file is the main game-loop for the game.
]]--

-- Import the player class
require "config"
require "gameboard"
require "math"

local inspect = require "inspect"

local config = Config           -- config.lua
local Player = require "player"
local SoundManager = require "sound_manager"
local gameboard = Gameboard     -- gameboard.lua

local theStick = require "shapes/the_stick"
local thePeriscope = require "shapes/the_periscope"
local theL = require "shapes/the_l"
local theSquare = require "shapes/the_square"
local theDog = require "shapes/the_dog"
local theT = require "shapes/the_t"
local theOtherDog = require "shapes/the_other_dog"

-- Class the holds all of the data for the GameLevel class
GameLevel = {
    --
    DEFAULT_CURSOR = {5, 1};
    
    inSession = false;
    gameOver = false;
    gameSpeed = 0.3;                    -- This is not a constant because you can change the game speed in-game
    turnTime = 0;       
    currentPiece = nil;
    savedPiece = nil;
    cursor = {};                       -- Since the shape outline is 4x4, this will be more-or-less centered
    currentPlayer = nil;
    sounds = nil;



    -- Initialize ():   Initializes local variables for the game object
    Initialize = function (self)
        math.randomseed (os.time ())
        self.currentPlayer = Player.new ()
        self.sounds = SoundManager.new()
    end;






    -- Play():  Sets the inSession variable which, in turn, causes the game to start in main.lua
    Play = function (self)
        gameboard:Initialize ()
        self.gameOver = false
        self:ResetCursor ()    
        self:GetNextPiece ()
        self.inSession = true
        self.savedPiece = nil
        self.currentPlayer.score = 0
    end;





    -- Update():    Main game loop
    Update = function (self, deltaTime)
        self.turnTime = self.turnTime - deltaTime

        if (self.rotateRequest) then
            self:RotatePiece ()
        end

        if (self.turnTime <= 0) then
            canMoveDown = self:CanMoveDown ()

            -- If we can move the current piece down, do so:
            if (canMoveDown) then
                self:MoveDown ()

            else
                -- Else, we need to set the current piece into the set tiles and get a new piece 
                if (self.cursor[2] > 4) then
                    tilesToSet = {}
                    for c = 1, 4 do
                        tilesToSet[c] = {
                            self.currentPiece.coords[c][1] + self.cursor[1] - 1,
                            self.currentPiece.coords[c][2] + self.cursor[2]
                        }
                    end
                    gameboard:SetTiles (tilesToSet, self.currentPiece.color)
                    self:ResetCursor ()
                    incrementAmount = gameboard:ProcessBreaks (0)

                    if (incrementAmount > 0) then
                        self.sounds:Play ('sfx_lineBreak')
                        self.currentPlayer:IncrementScore (incrementAmount)
                    end

                    self:GetNextPiece ()


            --  Else, game over!
                else
                    self:GameOver ()
                end
            end
    
            -- Reset the turn timer
            self.turnTime = self.gameSpeed
        end
    end;





    -- GetNextPiece():  Creates a new shape to drop
    GetNextPiece = function (self)
        self.currentPiece = nil
        pieceNumber = math.random (7)
        local piece = nil
        
        if pieceNumber == 1 then
            piece = theStick.new ()

        elseif pieceNumber == 2 then
            piece = thePeriscope.new ()

        elseif pieceNumber == 3 then
            piece = theL.new ()

        elseif pieceNumber == 4 then
            piece = theSquare.new ()

        elseif pieceNumber == 5 then
            piece = theDog.new ()

        elseif pieceNumber == 6 then
            piece = theT.new ()

        elseif pieceNumber == 7 then
            piece = theOtherDog.new ()

        else
            print ("ERROR: Attempted to create invalid piece (" .. pieceNumber .. ")")

        end

        self.currentPiece = piece
        -- self.currentPiece.coords = piece.coords
        -- self.currentPiece.color = piece.color
    end;




    -- SavePiece():     Stores the current piece to be used later 
    SavePiece = function (self)

        -- If there is no previously saved piece, save this one and get a new piece
        if (self.savedPiece == nil) then
            self.savedPiece = self.currentPiece
            self:ResetCursor ()
            self:GetNextPiece ()

        -- Else, swap out the previously saved piece and the current piece and reset the cursor to the default position
        else
            self:ResetCursor ()            
            swapPiece = self.currentPiece
            self.currentPiece = self.savedPiece
            self.savedPiece = swapPiece
            
        end
    end;




    -- ResetCursor():   Sets 'cursor' to the values of DEFAULT_CURSOR
    ResetCursor = function (self)
        self.cursor[1] = self.DEFAULT_CURSOR[1]
        self.cursor[2] = self.DEFAULT_CURSOR[2]
    end;




    -- RotatePiece():   Calls Rotate() on currentPiece and then makes sure that the resulting rotation doesn't go out of bounds
    RotatePiece = function (self)
        self.currentPiece:Rotate ()

        cursorMoveRequired = false
        
        for c = 1, 4 do
            if (self.cursor[1] + self.currentPiece.coords[c][1] - 1 > config.BOARD_WIDTH) then
                cursorMoveRequired = true
                break
            end
        end

        if (cursorMoveRequired) then
            corrected = false

            while (not corrected) do
                corrected = true
                self.cursor[1] = self.cursor[1] - 1
                
                for c = 1, 4 do
                    if (self.cursor[1] + self.currentPiece.coords[c][1] - 1 > config.BOARD_WIDTH) then
                        corrected = false
                        break
                    end
                end
            end
        end

        self.rotateRequest = false
    end;

    


    -- MoveDown():  Clears the space that currentPiece was in and moves the piece down one block
    MoveDown = function (self)
        -- Set the position of each square to one below its current position:
        self.cursor[2] = self.cursor[2] + 1
    end;




    -- CanMoveDown():   Performs collision detection on the current piece and returns true if no detections occurred
    CanMoveDown = function (self)
        -- Check if we got to bottom of the board        
        for c = 1, 4 do
            if (self.cursor[2] + self.currentPiece.coords[c][2] >= config.BOARD_HEIGHT) then
                return false
            end
        end

        -- Check for previously placed blocks
        for c = 1, 4 do
            if (gameboard:CheckTile ({
                self.cursor[1] + self.currentPiece.coords[c][1] - 1,
                self.cursor[2] + self.currentPiece.coords[c][2] + 1
            })) then
                return false
            end
        end

        return true
    end;





    -- GameOver():      
    GameOver = function (self)
        self.gameOver = true
    end;





    -- Quit():  Sets the inSession variable which, in turn, causes the game to exit to the main menu
    Quit = function (self)
        self.currentPiece = nil
        self.inSession = false
    end;




    -- Display():   Output the graphics of the game 
    Display = function (self)
        -- If GameOver, display the game over screen:
        if (self.gameOver) then
            love.graphics.setColor (config.shapeColors[5])

            love.graphics.print (
                "GAME OVER",
                (config.SCREEN_WIDTH * (1 / 3)),
                (config.SCREEN_HEIGHT / 2) - 100,
                0,
                10,
                10
            )   

            love.graphics.print (
                "Press <ENTER> to play again or <ESCAPE> to exit to the Main Menu",
                (config.SCREEN_WIDTH / 3) - 100,
                (config.SCREEN_HEIGHT * (2 / 3)) - 100,
                0,
                2,
                2
            )
        
        
        
            
        -- Else, continue with regular 
        else
            -- Set color to the default white.
            love.graphics.setColor (config.gameColors["Menu_Text"])

            -- Scoreboard
            love.graphics.print (
                "SCORE:\t\t" .. tostring(self.currentPlayer.score),
                (config.SCREEN_WIDTH * (2 / 3)) + 250,
                (config.SCREEN_HEIGHT / 2) - 50,
                0,
                2,
                2
            )

            -- Saved Piece (BOX)
            love.graphics.rectangle (
                'line',
                (config.SCREEN_WIDTH * (2 / 3)) + 275,
                50,
                250,
                250
            )

            -- Saved Piece (SHAPE)
            if (self.savedPiece ~= nil) then
                love.graphics.setColor (config.shapeColors[self.savedPiece.color])
                for c=1, 4 do 
                    love.graphics.rectangle (
                        'fill',
                        gameboard.CELL_WIDTH * (self.savedPiece.coords[c][1]) + ((config.SCREEN_WIDTH * (2 / 3)) + 275),
                        (gameboard.CELL_HEIGHT * self.savedPiece.coords[c][2]) + 80,
                        gameboard.CELL_WIDTH,
                        gameboard.CELL_HEIGHT
                    )                        
                end
            end

            -- Display gameboard
            gameboard:Display ()
            
            -- Display moving piece
            -- (Because the default coords are all numbers 1-4, we need to correct the offset to include the 'cursor)        
            love.graphics.setColor (config.shapeColors[self.currentPiece.color])

            for c=1, 4 do 
                love.graphics.rectangle (
                    'fill',
                    gameboard.CELL_WIDTH * (self.cursor[1] + self.currentPiece.coords[c][1] - 2) + gameboard.START_X,
                    gameboard.CELL_HEIGHT * (self.cursor[2] + self.currentPiece.coords[c][2] - 1),
                    gameboard.CELL_WIDTH,
                    gameboard.CELL_HEIGHT
                )                        
            end
        end
    end;
}