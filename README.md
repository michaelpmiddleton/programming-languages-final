# __CSC 323: Programming Languages - Final Project__  

This repository comprises the files for the "Programming Languages" (CSC-323) course final project for my Spring 2018 semester at Stonehill College.
![Screenshot of the final project.](http://michaelpmiddleton.com/res/img/programming-languages-final.png)
</div>
</br>

## _Overview:_  
*Coming Soon...*
 

</br>  

## _Want to Fork?_    
Please feel free to use my code as a reference for your own work. With that said, at this time I am requesting that you do not _copy_ this code. Thank you for your understanding and happy hacking!  

</br></br>

## _Got Questions?_ 
I'm happy to answer them! Just [send me an email](mailto:mp.middleton@outlook.com)!
